# Simple Stack Virtual Machine

A simple pushdown stack virtual machine, developed to be a compiler
target for students of a first time compiler course. The intention is
simplicity over advanced features.

The implementation is in Haskell and currently the only supported input format
is text, as defined by the grammar below, although it would
straightforward to define a binary format.

Contact benedict.gaster at uwe.ac.uk for any questions.


# Requirements

GHC 7.8.3 greater

Install the Haskell Platform if you are not sure howelse to use GHC:

   [https://www.haskell.org/platform/]

There are a few packages required and I recommend simply building in a sandbox.

# Building

Create sandbox

```cabal sandbox init```

Install any dependencies

```cabal install --only-dependencies```

Then configure and build

```cabal configure```

```cabal build```

A single binary is produced

```./dist/build/smachine/smachine```

# Usage

The simulator takes a list of input files, which are concaternated
together to produce a program to simulate.

```
Usage: smachine [-vh] [file...]
```

The options `-v` and `-h` print the version and help, respectively.

Thus to run (simulate) an example input program (ex1.smac) run:

```./dist/build/smachine/smachine examples/ex1.smac```

Any program is expected to have an single entry point:

```
   main:
```

and all programs must terminate with the instruction:

```
   HALT
```

# Examples

## Hello World

```
main:
  PUSH "World" ;
  PUSH "Hello," ;
  OUT ;
  OUTLN ;
  HALT ;
```

Run this example with the command:

```
./dist/build/smachine/smachine examples/ex3.smac
```
and you will see the output:

```
Hello,World
number of instructions: 6
```

Note that the number instructions is actually one more than you might
expect, this is because there is a jump to the entry point `main:`.

## Defining variables

There is no explict commands for defining storage, however, a simple
idiom can be used to acheive storage allocation. Simple define a label
followed by a NOP and then reference that location as storage. For
example, to define a variable 'x':

```
x:
  NOP ;
```

then to store the value 10 to the location referenced by 'x':

```
  &x;      // push address of x on stack
  PUSH 10; // push 10 on the stack
  STORE ; // store 10 in location x (poping both values from stack)
```

Loading a value is simply a matter of putting the address on the stack
and issueing a load:

```
  &x ;   // push address of x on stack
  LOAD ; // load value from address on top of stack and place on stack
```

Putting this all togther a simple example to initalize 'x' to 10 and
then print the value stored at location 'x' might be written as:

```
x:
  NOP ;

main:
  &x ;
  PUSH 10 ;
  STORE ;

  PUSH "x = " ;
  OUT ;

  &x ;
  LOAD ;
  OUTLN ;
  HALT ;
```

Run this example with the command:

```
./dist/build/smachine/smachine examples/ex5.smac
```
and you will see the output:

```
x = 10
number of instructions: 10
```

# Input Grammar

```
module : comment* | inst ';' comment?

comment: `-- .*\n
       | '{-' .* '-}'

inst : `NOP
     | `HALT`
     | `PUSH` literal
     | `POP`
     | `DUP`
     | `SWAP`
     | `DUMPSTACK`
     | `OUT`
     | `OUTLN`
     | `IN`
     | labeldef
     | `&`id
     | `STORE`
     | `LOAD`
     | `EQ`
     | `NEQ`
     | `LEQ`
     | `GEQ`
     | `LT`
     | `GT`
     | `COMPL`
     | `MINUS`
     | `ADD`
     | `DIV`
     | `REM`
     | `MUL`
     | `AND`
     | `OR`
     | `NOT`
     | `JMPNZ`
     | `JMP`
     | `SUBCALL`
     | `RETURN`

labeldef : id `:`

literal : int | double | string

int : [0-9]+

double : [0-9]+'.'[0-9]+

string : \"[a-b0-9A-b]*\"

id : [a-b][a-b0-9A-b]*
```

# Instruction Set

The machines instruction set is very simple, although it does support
a few "ease" of use features, such as take the address of a location.

### Machine control

```
  NOP                       do nothing
  HALT                      stop machine
```

### Stack control

```
 PUSH LITERAL              push a literal value onto top of stack
 POP                       pop a value from the top of stack
 DUP                       duplicate the value on top of the stack
 SWAP                      swap the two top values of the stack
 DUMPSTACK                 output the contents of stack to stdout (for debugging)
```

### IO

```
 OUT                       pop A, write A to stdout
 OUTLN                     pop A, write A to stdout and newline
 IN                        read an int from stdin and put on the top of stack
```

### Locations and addresses

```
 name:                     name a location in the input stream
 &name                     take the address of a named location and push on stack
```

### Memory operations

```
 STORE                     pop A, pop B, write B to address A
 LOAD                      pop A, load value from address A and push
```

### Integer operations

```
 EQ                        pop A, pop B, push 0, if A == B, otherwise push 1
 NEQ                       pop A, pop B, push 0, if A != B, otherwise push 1
 LEQ                       pop A, pop B, push 0, if A <= B, otherwise push 1
 GEQ                       pop A, pop B, push 0, if A >= B, otherwise push 1
 LT                        pop A, pop B, push 0, if A < B, otherwise push 1
 GT                        pop A, pop B, push 0, if A > B, otherwise push 1
 COMPL                     pop A, push ~A
 MINUS                     pop A, pop B, push A - B
 ADD                       pop A, pop B, push A + B
 MUL                       pop A, pop B, push A * B
 DIV                       pop A, pop B, push A quot B
 REM                       pop A, pop B, push A rem B
 AND                       pop A, pop B, push A & B
 OR                        pop A, pop B, push A | B
 NOT                       pop A, push !A
```

### Control Flow

```
 JMPNZ                     pop A, pop B, if a != 0, then transfer control to B
 JMP                       pop A, then transfer control to A (i.e. set PC)
 SUBCALL                   push next location (PC+1) on to return stack,
                           pop A, transfer control (i.e. set PC) to A
 RETURN                    pop return stack as R, transfer control (i.e. set PC)
                           to R
```			     
