module Main where

import System.Environment
import System.Exit
import System.IO

import Types
import Parser
import Eval
import Control.Exception

import Data.Maybe

readProgram :: FilePath -> IO (Maybe String)
readProgram filename =
  do strOrExc <- try (readFile filename) ::
                                              IO (Either SomeException String)
     case strOrExc of
          Left except -> print except >> return Nothing
          Right s     -> return $ Just s

parse ["-h"] = usage   >> return Nothing
parse ["-v"] = version >> return Nothing
parse []     = usage   >> return Nothing
parse fs     = do progs <- mapM readProgram fs
                  return $ c $ catMaybes progs
  where c []    = Nothing
        c progs = Just $ concat progs

usage   = putStrLn "Usage: smachine [-vh] [file...]"
version = putStrLn "Simple Stack Machine 0.1"

main :: IO ()
main = do hSetBuffering stdout NoBuffering
          args <- getArgs >>= parse
          maybe (return ()) (\prog -> do cmds <- readCommands prog
                                         res  <- eval cmds
                                         case res of
                                              Left str -> do putStrLn str
                                                             return ()
                                              Right icount  ->
                                                    putStrLn ("number of instructions: " ++ show icount)) args
