{-
-}

module Eval where

import Data.Bits
import qualified Data.Map.Strict as M
import System.IO

import Types

type Stack a = [a]

type ReturnStack = Stack Int
type RunStack    = Stack Literal

--------------------------------------------------------
-- Non Monad version
--------------------------------------------------------

findLabel'' :: String -> Name -> LabelMap -> Int
findLabel'' msg n lmap =
  case M.lookup n lmap of
      Just a -> a
      _      -> error msg

evaluator :: Program -> IO ()
evaluator (Program p lmap) =
  eval' p (drop (findLabel'' "main not defined" "main" lmap) p) lmap [] []

eval' :: [Command] ->
           [Command] ->
             LabelMap ->
               ReturnStack ->
                 RunStack -> IO ()

eval' _ [] _ _ _ =  error "unexpected end of program, i.e. no HALT"

eval' fprog (Nop:rem) lmap rets runs = eval' fprog rem lmap rets runs

eval' _     (Halt:_) _ _ _  = return ()

eval' fprog (Push lit:rem) lmap rets runs = eval' fprog rem lmap rets (lit:runs)

eval' _ (Pop:_) _ _ [] = error "tried to POP from empty stack"

eval' fprog (Pop:rem) lmap rets (_:runs) = eval' fprog rem lmap rets runs

eval' fprog (Label _ _:rem) lmap rets runs = eval' fprog rem lmap rets runs

eval' _ (Out:_) _ _ [] = error "tried OUT from empty stack"

eval' fprog (Out:rem) lmap rets (v:runs) = do print v
                                              eval' fprog rem lmap rets runs

eval' fprog (In:rem) lmap rets runs = do i <- readLn :: IO Int
                                         eval' fprog rem lmap rets (LitI i:runs)

eval' fprog (Dup:rem) lmap rets (a:runs) =
  eval' fprog rem lmap rets (a:a:runs)

eval' _ (Dup:_) _ _ _ = error "Dup expects one int on the stack"

eval' fprog (Swap:rem) lmap rets (a:b:runs) =
  eval' fprog rem lmap rets (b:a:runs)

eval' _ (Swap:_) _ _ _ = error "SWAP expects two values on the stack"


eval' fprog (Comp op:rem) lmap rets (a:b:runs) =
  eval' fprog rem lmap rets (c a b:runs)
      where c i i' = LitI (compareOp "" op i i')

eval' _ (Comp op:_) _ _ _ = error (show op ++ " expects two ints on the stack")

eval' fprog (Add:rem) lmap rets (a:b:runs) =
  eval' fprog rem lmap rets (c a b:runs)
      where c (LitI i) (LitI i') = LitI (i + i')
            c _        _         = error "ADD expects two ints on the stack"

eval' _ (Add:_) _ _ _ = error "ADD expects two ints on the stack"

eval' fprog (Minus:rem) lmap rets (a:b:runs) =
  eval' fprog rem lmap rets (c a b:runs)
      where c (LitI i) (LitI i') = LitI (i - i')
            c _        _         = error "MINUS expects two ints on the stack"

eval' _ (Minus:_) _ _ _ = error "MINUS expects two ints on the stack"

eval' fprog (And:rem) lmap rets (a:b:runs) =
  eval' fprog rem lmap rets (c a b:runs)
      where c (LitI i) (LitI i') = LitI (i .&. i')
            c _        _         = error "AND expects two ints on the stack"

eval' _ (And:_) _ _ _ = error "AND expects two ints on the stack"

eval' fprog (Or:rem) lmap rets (a:b:runs) =
  eval' fprog rem lmap rets (c a b:runs)
      where c (LitI i) (LitI i') = LitI (i .|. i')
            c _        _         = error "OR expects two ints on the stack"

eval' _ (Or:_) _ _ _ = error "OR expects two ints on the stack"

eval' fprog (Not:rem) lmap rets (a:runs) =
  eval' fprog rem lmap rets (c a:runs)
      where c (LitI i) = LitI (complement i)
            c _        = error "NOT expects one int on the stack"

eval' _ (Not:_) _ _ _ = error "NOT expects one int on the stack"

eval' fprog (JmpNZ:rem) lmap rets (addr:a:runs) = c a addr
      where c (LitI 0) _ = eval' fprog rem lmap rets runs
            c _        a =
              eval' fprog
                    (drop (unlit1 a "JMPNZ expects one int on the stack") fprog)
                    lmap
                    rets
                    runs

eval' _ (JmpNZ:_) _ _ _ =
  error "JMPNZ expects one int and an address on the stack"

eval' fprog (Jmp:_) lmap rets (a:runs) =
  eval' fprog
        (drop (unlit1 a "JMP expects an address on the stack") fprog)
        lmap rets runs

eval' _ (Jmp:_) _ _ _ = error "JMP expects an address on the stack"

eval' fprog (Subcall:rem) lmap rets (a:runs) =
  eval' fprog
        (drop (unlit1 a "SUBCALL expects an address on the stack") fprog)
        lmap (length fprog - length rem:rets) runs
        
eval' _ (Subcall:_) _ _ _ = error "SUBCALL expects an address on the stack"

eval' fprog (Return:_) lmap (a:rets) runs =
  eval' fprog
        (drop a fprog)
        lmap rets runs

eval' _ (Return:_) _ _ _ = error "Return expects an address on the return stack"

eval' fprog (AddrOf n:rem) lmap rets runs =
  case M.lookup n lmap of
       Just a -> eval' fprog rem lmap rets (LitI a:runs)
       _      -> error ("label " ++ n ++ " not found in ADDROF" ++ show lmap)

eval' fprog (Store:rem) lmap rets (b:addr:runs) =
      let a = unlit1 addr "STORE expects an address and a value on the stack"
          p = take (a + 1) fprog ++ [Lit b] ++ drop (a + 2) fprog
      in eval' p rem lmap rets runs

eval' _ (Store:_) _ _ runs =
  error "Store expects an address and a vvvalue on the return stack"

eval' fprog (Load:rem) lmap rets (addr:runs) =
      let a     = unlit1 addr "LOAD expects an address on the stack"
          Lit v = head $ drop (a + 1) fprog
      in eval' fprog rem lmap rets (v:runs)

eval' _ (Load:_) _ _ _ =
  error "Load expects an address on the return stack"         

-- currently only eval' adds literals to the command stream, but this
-- should be updated to allow programs to contain literals
eval' fprog (Lit _:rem) lmap rets runs =
  eval' fprog rem lmap rets runs

eval' _ (c:_) _ _ _ = error ("unimpemented instruction: " ++ show c)
