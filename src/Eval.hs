{-# LANGUAGE FlexibleContexts #-}

module Eval where

import Data.Bits
import qualified Data.Map.Strict as M
import System.IO
import Control.Monad.Trans
import Control.Monad.State.Lazy
import Control.Monad.Except
import Control.Monad.Trans.Except

import Types

type Stack a = [a]

type ReturnStack = Stack Int
type RunStack    = Stack Literal
type Locals      = Stack Literal
type LocalsStack = Stack Locals
type Args        = Stack Literal
type ArgsStack   = Stack Args

data ST = ST { program    :: [Command],
               lmap       :: LabelMap,

               retstack   :: ReturnStack,
               runstack   :: RunStack,

               locals      :: Locals,
               localsstack :: LocalsStack,

               calleeargs  :: Args,
               callerargs  :: Args,
               argsstack  :: ArgsStack,

               returnReg   :: Maybe Literal,

               instCount  :: Int
             }

type ExSTM = StateT ST (ExceptT String IO)

-- Functions for working within the ST transformer

input :: ExSTM Int
input = liftIO (readLn :: IO Int)

output :: (String -> IO ()) -> Literal -> ExSTM ()
output op = liftIO . op . show

store :: String -> Literal -> Literal -> Literal -> ExSTM ()
store msg (LitI a) (LitI idx) v =
  do st0 <- get
     let fprog = program st0
         prog = take ((a+idx) + 1) fprog ++ [Lit v] ++ drop ((a+idx) + 2) fprog
     put (st0 { program = prog } )

store msg LitLocalsAddress (LitI idx) v = storeLocal idx v

store msg _ _ _ = throwError msg

load :: String -> Literal -> Literal -> ExSTM Literal
load msg (LitI a) (LitI idx) =
  do st0 <- get
     v <- check $ head $ drop ((a+idx) + 1) (program st0)
     return v
  where check (Lit v) = return v
        check _       = throwError msg

load msg LitArgsAddress (LitI idx) =
  do v <- loadArg idx
     return v

load msg LitLocalsAddress (LitI idx) =
  do v <- loadLocal idx
     return v

load msg _ _ = throwError msg

getProgram :: ExSTM [Command]
getProgram = do st <- get
                return $ program st

lookupLabel :: String -> Name -> ExSTM Int
lookupLabel msg n = do st <- get
                       l <- findLabel' msg n (lmap st)
                       return l

findLabel' :: String -> Name -> LabelMap -> ExSTM Int
findLabel' msg n lmap =
  case M.lookup n lmap of
      Just a -> return a
      _      -> throwError msg

putRunStack :: RunStack -> ExSTM ()
putRunStack rs = do st0 <- get
                    put (st0 { runstack = rs } )

putRetStack :: ReturnStack -> ExSTM ()
putRetStack rs = do st0 <- get
                    put (st0 { retstack = rs } )

pushRunStack :: Literal -> ExSTM ()
pushRunStack v = do st0 <- get
                    putRunStack (v:runstack st0)

getRunStack :: ExSTM RunStack
getRunStack = get >>= return . runstack

pushRetStack :: Int -> ExSTM ()
pushRetStack v = do st0 <- get
                    putRetStack $ v:retstack st0

pop1Stack :: String -> (ST -> Stack a) -> (Stack a -> ExSTM ()) -> ExSTM a
pop1Stack msg sel p = do st <- get
                         let s = sel st
                         if (length s) == 0
                            then throwError msg -- stack is empty
                            else do p (tail s)
                                    return $ head s

pop1RunStack :: String -> ExSTM Literal
pop1RunStack msg = pop1Stack msg runstack putRunStack

pop1RetStack :: String -> ExSTM Int
pop1RetStack msg = pop1Stack msg retstack putRetStack

pop2Stack :: String -> (ST -> Stack a) -> (Stack a -> ExSTM ()) -> ExSTM (a,a)
pop2Stack msg sel p = do st <- get
                         let s = sel st
                         if (length s) < 2
                            then throwError msg
                            else do p (tail $ tail s)
                                    return $ (head s, head $ tail s)

pop2RunStack :: String -> ExSTM (Literal,Literal)
pop2RunStack msg = pop2Stack msg runstack putRunStack

pop2RetStack :: String -> ExSTM (Int,Int)
pop2RetStack msg = pop2Stack msg retstack putRetStack

incInstCount :: ExSTM ()
incInstCount = do st <- get
                  put (st { instCount = instCount st + 1 })

getLocals :: ExSTM Locals
getLocals = get >>= return . locals

getLocalsStack :: ExSTM LocalsStack
getLocalsStack = get >>= return . localsstack

popLocalsStack :: ExSTM ()
popLocalsStack =
  do st <- get
     put (st { localsstack = tail (localsstack st), locals = head (localsstack st) })

pushLocals :: ExSTM ()
pushLocals =
  do st <- get
     put (st { localsstack = (locals st) : localsstack st, locals = [] })

storeLocal :: Int -> Literal -> ExSTM ()
storeLocal n lit =
  do st <- get
     put (st { locals = (take n (locals st) ++ [lit] ++ drop (n+1) (locals st)) })

loadLocal :: Int -> ExSTM Literal
loadLocal n = getLocals >>= return . (!!n)

getCalleeArgs :: ExSTM Args
getCalleeArgs = get >>= return . calleeargs

getArgsStack :: ExSTM ArgsStack
getArgsStack = get >>= return . argsstack

popArgs :: ExSTM ()
popArgs =
  do st <- get
     put (st { argsstack = tail (argsstack st),
               calleeargs = head (argsstack st),
               callerargs = [] })

pushArgs :: ExSTM ()
pushArgs =
  do st <- get
     put (st { argsstack = (calleeargs st) : argsstack st,
               calleeargs = reverse $ callerargs st,
               callerargs = [] })

loadArg :: Int -> ExSTM Literal
loadArg n = getCalleeArgs >>= return . (!!n)

pushArg :: Literal -> ExSTM ()
pushArg lit =
  do st <- get
     put (st { callerargs = lit : callerargs st })

getInstCount :: ExSTM Int
getInstCount = get >>= return . instCount

getRetReg :: String -> ExSTM Literal
getRetReg msg =
  do st <- get
     case returnReg st of
          Just l -> return l
          Nothing -> throwError msg

putRetReg :: Maybe Literal -> ExSTM ()
putRetReg lit =
  do st <- get
     put (st { returnReg = lit })

------------------------------------------------------------
-- Some helper functions
------------------------------------------------------------

unlit1 (LitI a) _   = return a
unlit1 _        msg = throwError msg

compareOp :: String -> CompOp -> Literal -> Literal ->ExSTM Int
compareOp _ op (LitI i) (LitI i') = return $ fromBool $ compareOp' op
  where compareOp' CEq  = i == i'
        compareOp' CNEq = i /= i'
        compareOp' CLEq = i <= i'
        compareOp' CGEq = i >= i'
        compareOp' CLT  = i < i'
        compareOp' CGT  = i > i'

compareOp msg _ _ _ = throwError msg

arithBinaryOp :: String ->
                   (Int -> Int -> Int) ->
                     Literal ->
                       Literal ->
                         ExSTM Int
arithBinaryOp _ op (LitI i) (LitI i') = return $ op i i'
arithBinaryOp msg _ _ _ = throwError msg

arithUnaryOp :: String ->
                   (Int -> Int) ->
                     Literal ->
                       ExSTM Int
arithUnaryOp _ op (LitI i) = return $ op i
arithUnaryOp msg _ _ = throwError msg

fromBool :: Bool -> Int
fromBool True  = 0
fromBool False = 1

----------------------------------------------------
-- Monad Based Evaluator
----------------------------------------------------

eval :: Program -> IO (Either String Int)
eval (Program p lmap) =
  runExceptT $ evalStateT (do l <- findLabel' "main not defined"
                                              "main"
                                              lmap
                              eval' $ drop l p
                              getInstCount)
                          (ST p lmap [] [] [] [] [] [] [] Nothing 0)

eval' :: [Command] -> ExSTM ()
eval' prog = incInstCount >> eval'' prog

eval'' :: [Command] -> ExSTM ()

eval'' [] =  throwError "unexpected end of program, i.e. no HALT"

eval'' (Nop:rem)      = eval' rem

eval'' (Halt:_)       = return ()

eval'' (Push lit:rem) = pushRunStack lit >> eval' rem

eval'' (Pop:rem)      =
  pop1RunStack "tried to POP from empty stack" >>  eval' rem

eval'' (DumpStack:rem) =
  do  rs <- getRunStack
      liftIO $ putStr $ (show rs ++ "\n")
      eval' rem

eval'' (Label _ _:rem) = eval' rem

eval'' (Out:rem) =
  do v <- pop1RunStack "tried OUT from empty stack"
     output putStr v
     eval' rem

eval'' (OutLN:rem) =
  do v <- pop1RunStack "tried OUTLN from empty stack"
     output putStrLn v
     eval' rem

eval'' (In:rem) =
  do v <- input
     pushRunStack $ LitI v
     eval' rem

eval'' (Flush:rem) =
  do liftIO $ hFlush stdout
     eval' rem

eval'' (PushArg:rem) =
  do v <- pop1RunStack "tried PUSHARG from empty stack"
     pushArg v
     eval' rem

eval'' (MovRet:rem) =
  do v <- pop1RunStack "tried MOVRET from empty stack"
     putRetReg $ Just v
     eval' rem

eval'' (PushRet:rem) =
  do v <- getRetReg "tried to read undefined return reg"
     pushRunStack v
     eval' rem

eval'' (Dup:rem) =
  do v <- pop1RunStack "DUP expects one int on the stack"
     pushRunStack v
     pushRunStack v
     eval' rem

eval'' (Swap:rem) =
  do (a,b) <- pop2RunStack "SWAP expects two values on the stack"
     pushRunStack a
     pushRunStack b
     eval' rem

eval'' (Comp op:rem) =
  do let msg = "COMP expexts two ints on the stack"
     (a,b) <- pop2RunStack msg
     res <- compareOp msg op a b
     pushRunStack $ LitI res
     eval' rem

eval'' (Compl:rem) =
  do let msg = "COMPL expexts one int on the stack"
     a <- pop1RunStack msg
     res <- compareOp msg CEq a (LitI 1)
     pushRunStack $ LitI res
     eval' rem

eval'' (Add:rem) =
  do let msg = "ADD expects two ints on the stack"
     (a,b) <- pop2RunStack msg
     res <- arithBinaryOp msg (+) a b
     pushRunStack $ LitI res
     eval' rem

eval'' (Minus:rem) =
  do let msg = "MINUS expects two ints on the stack"
     (a,b) <- pop2RunStack msg
     res <- arithBinaryOp msg (-) a b
     pushRunStack $ LitI res
     eval' rem

eval'' (Mul:rem) =
  do let msg = "Mul expects two ints on the stack"
     (a,b) <- pop2RunStack msg
     res <- arithBinaryOp msg (*) a b
     pushRunStack $ LitI res
     eval' rem

eval'' (Div:rem) =
  do let msg = "Div expects two ints on the stack"
     (a,b) <- pop2RunStack msg
     res <- arithBinaryOp msg (quot) a b
     pushRunStack $ LitI res
     eval' rem

eval'' (And:rem) =
  do let msg = "AND expects two ints on the stack"
     (a,b) <- pop2RunStack msg
     res <- arithBinaryOp msg (.&.) a b
     pushRunStack $ LitI res
     eval' rem

eval'' (Or:rem) =
  do let msg = "AND expects two ints on the stack"
     (a,b) <- pop2RunStack msg
     res <- arithBinaryOp msg (.|.) a b
     pushRunStack $ LitI res
     eval' rem

eval'' (Not:rem) =
  do let msg = "NOT expects one int on the stack"
     a <- pop1RunStack msg
     res <- arithUnaryOp msg complement a
     pushRunStack $ LitI res
     eval' rem

eval'' (JmpNZ:rem) =
  do let msg = "JMPNZ expects one int and an address on the stack"
     (addr, a) <- pop2RunStack msg
     if a == LitI 0
        then eval' rem
        else do addr' <- unlit1 addr msg
                prog <- getProgram
                eval' $ drop addr' prog

eval'' (Jmp:_) =
  do let msg = "JMP expects an address on the stack"
     a <- pop1RunStack msg
     a' <- unlit1 a msg
     prog <- getProgram
     eval' $ drop a' prog

eval'' (Subcall:rem) =
  do let msg = "SUBCALL expects an address on the stack"
     a <- pop1RunStack msg
     a' <- unlit1 a msg
     prog <- getProgram
     pushRetStack ((length prog - length rem))
     pushArgs
     eval' $ drop a' prog

eval'' (Return:_) =
  do let msg = "RETURN expects an address on the return stack"
     a <- pop1RetStack msg
     popArgs
     prog <- getProgram
     eval' (drop a prog)

eval'' (AddrOf n:rem) =
  do let msg = "Label " ++ n ++ " not found in ADDROF"
     l <- lookupLabel msg n
     pushRunStack $ LitI l
     eval' rem

eval'' (Store:rem) =
  do let msg = "STORE expects one int and an address on the stack"
     (b,addr) <- pop2RunStack msg
     store msg addr (LitI 0) b
     eval' rem

eval'' (StoreIdx:rem) =
  do let msg = "STORE expects one int and an address on the stack"
     (idx,addr) <- pop2RunStack msg
     b          <- pop1RunStack msg
     store msg addr idx b
     eval' rem

eval'' (Load:rem) =
  do let msg = "LOAD expects an address on the stack"
     a <- pop1RunStack msg
     v <- load msg a (LitI 0)
     pushRunStack v
     eval' rem

eval'' (LoadIdx:rem) =
  do let msg = "LOAD expects an address on the stack"
     (idx, a) <- pop2RunStack msg
     v <- load msg a idx
     pushRunStack v
     eval' rem

eval'' (cmd:rem) =
  throwError ("unimplemented instruction " ++ show cmd)
