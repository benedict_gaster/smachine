{-# LANGUAGE FlexibleInstances #-}

{-

 Types for a our simple stack machine. The instructions of which are defined as:

 Machine control

 NOP                       do nothing
 HALT                      stop machine

 Stack control

 PUSH LITERAL              push a literal value onto top of stack
                           literals can be:
                              integer
                              char
                              double
                              symbol LOCALS, which represents the locals pointer for a function
                              symbol ARGS, which represents the args pointer for a function
 POP                       pop a value from the top of stack
 DUP                       duplicate the value on top of the stack
 SWAP                      swap the two top values of the stack

 Return Register

 MOVRET                    push return register onto top of stack
 PUSHRET                   pop A, set return register to A

 Arguments

 ALLOCA                    pops A, allocate A space in the local array
 PUSHARG                   pop A, pushes A onto the argument stack for the next subroutine call
                           (note the callee argument stack is empty on entry to the function and
                            cleared on returning from a SUBCALL)

 IO

 OUT                       pop A, write A to stdout
 IN                        read an int from stdin and put on the top of stack

 Locations and addresses

 name:                     name a location in the input stream
 &name                     take the address of a named location and push on stack

 Memory operations

 STORE                     pop A, pop B, write B to address A
 STOREIDX                  pop A, pop B, pop C, write C to address (A+B)
 LOAD                      pop A, load value from address A and push
 LOADIDX                   pop A, pop B, load value from address (A+B) and push

                           Note: address can point into the instruction stream, or
                           be stack addresses, i.e. index into the stack

 Integer operations

 EQ                        pop A, pop B, push 0, if A == B, otherwise push 1
 LEQ                       pop A, pop B, push 0, if A <= B, otherwise push 1
 GEQ                       pop A, pop B, push 0, if A >= B, otherwise push 1
 LT                        pop A, pop B, push 0, if A < B, otherwise push 1
 GT                        pop A, pop B, push 0, if A > B, otherwise push 1

 COMPL                     pop A, push ~A
 MINUS                     pop A, pop B, push A - B
 ADD                       pop A, pop B, push A + B
 MUL                       pop A, pop B, push A * B
 DIV                       pop A, pop B, push A / B
 AND                       pop A, pop B, push A & B
 OR                        pop A, pop B, push A | B
 NOT                       pop A, push !A

 Control Flow

 JMPNZ                     pop A, pop B, if a != 0, then transfer control to B
 JMP                       pop A, then transfer control to A (i.e. set PC)
 SUBCALL                   push next location (PC+1) on to return stack,
                           pop A, transfer control (i.e. set PC) to A,
                           allocates argument array in callee and copies arguments (PUSHARG)
 RETURN                    pop return stack as R, transfer control (i.e. set PC)
                           to R
                           callee's argument array local arrays are reclaimed
                           caller's call argument stack is reclaimed

-}
module Types where

import PPrint
import qualified Data.Map.Strict as M

type Name = String

data Literal = LitI Int
             | LitC Char
             | LitS String
             | LitArgsAddress
             | LitLocalsAddress
      deriving Eq

data CompOp =  CEq
             | CLEq
             | CGEq
             | CLT
             | CGT
             | CNEq


type LabelMap = M.Map String Int
data Program = Program [Command] LabelMap

data Command = Nop
             | Halt
             | Lit Literal    -- we allow literals to be stored in the command
                              -- stream (although currently this is not supported
                              -- by the parser, just the evaluator!
             | Push Literal
             | Pop
             | Out
             | OutLN
             | In
             | Flush
             | DumpStack      -- Dump the stack to stdout
             | Label Name Int -- this is not really a command per say, but...
                              -- the Int represents the index of the label
             | Store
             | StoreIdx
             | Load
             | LoadIdx

             | MovRet
             | PushRet
             | AllocA
             | PushArg

             | Compl
             | Dup
             | Swap
             | Comp CompOp
             | Minus
             | Add
             | Mul
             | Div
             | Rem
             | And
             | Or
             | Not
             | JmpNZ
             | Jmp
             | Subcall
             | Return
             | AddrOf Name -- push the address of a label

--------------------
-- Pretty Printing
--------------------
instance Pretty Literal where
  pretty lit = ppLiteral lit

instance Pretty CompOp where
  pretty lit = ppCompOp lit

instance Pretty Command where
  pretty command = ppCommand command

instance Show Literal where
  show lit = show (pretty lit)

instance Show CompOp where
  show lit = show (pretty lit)

instance Show Command where
  show command = show (pretty command)

instance Pretty Program where
  pretty (Program p _) = vcat $ map pretty p

instance Show Program where
  show = show . pretty

ppLiteral :: Literal -> Doc
ppLiteral (LitI i)         = pretty i
ppLiteral (LitC c)         = pretty c
ppLiteral (LitS s)         = text s
ppLiteral LitArgsAddress   = text "ARGS"
ppLiteral LitLocalsAddress = text "LOCALS"

ppCompOp :: CompOp -> Doc
ppCompOp CEq          = ppHelper "EQ"
ppCompOp CNEq         = ppHelper "NEQ"
ppCompOp CGEq         = ppHelper "GEQ"
ppCompOp CLT          = ppHelper "LT"
ppCompOp CGT          = ppHelper "GT"


ppCommand :: Command -> Doc
ppCommand Nop         = ppHelper "NOP"
ppCommand Halt        = ppHelper "HALT"
ppCommand Return      = ppHelper "RETURN"
ppCommand (Push lit)  = ppHelper "PUSH" <+> pretty lit
ppCommand (Lit lit)   = indent 1 $ pretty lit
ppCommand Pop         = ppHelper "POP"
ppCommand Compl       = ppHelper "COMPL"
ppCommand Out         = ppHelper "OUT"
ppCommand OutLN       = ppHelper "OUTLN"
ppCommand In          = ppHelper "IN"
ppCommand Flush       = ppHelper "FLUSH"
ppCommand (Label n _) = text n <> text ":"
ppCommand Store       = ppHelper "STORE"
ppCommand StoreIdx    = ppHelper "STOREIDX"
ppCommand Load        = ppHelper "LOAD"
ppCommand LoadIdx     = ppHelper "LOADIDX"
ppCommand MovRet      = ppHelper "MOVRET"
ppCommand PushRet     = ppHelper "PUSHRET"
ppCommand AllocA      = ppHelper "ALLOCA"
ppCommand PushArg     = ppHelper "PUSHARG"
ppCommand (Comp op)   = pretty op
ppCommand Minus       = ppHelper "MINUS"
ppCommand Mul         = ppHelper "MUL"
ppCommand Div         = ppHelper "DIV"
ppCommand Rem         = ppHelper "REM"
ppCommand Dup         = ppHelper "DUP"
ppCommand Swap        = ppHelper "SWAP"
ppCommand Add         = ppHelper "ADD"
ppCommand And         = ppHelper "AND"
ppCommand Or          = ppHelper "OR"
ppCommand Not         = ppHelper "Not"
ppCommand JmpNZ       = ppHelper "JMPNZ"
ppCommand Jmp         = ppHelper "JMP"
ppCommand Subcall     = ppHelper "SUBCALL"
ppCommand (AddrOf n)  = indent 1 $ text "&" <> text n


ppHelper = indent 1 . text
