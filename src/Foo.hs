data F = F { one :: Int, two :: Bool }

selSomething :: (F -> a) -> F -> a
selSomething fun f = fun f

buildSomething :: (a -> F) -> a -> F
buildSomething fun a = fun a
