{-
 Parse a simple stack machine


-}

module Parser where

import Data.Char
import Text.ParserCombinators.Parsec
import qualified Text.ParserCombinators.Parsec.Token as P
import Text.ParserCombinators.Parsec.Char
import Text.ParserCombinators.Parsec.Language( haskellDef )
import Control.Monad
import qualified Data.Map.Strict as M

import Types

type Parse a = CharParser St a
data St = St { cmdPosition :: Int, lmap :: LabelMap }

readCommands :: String -> IO Program
readCommands input =
  case runParser (wrap commands) (St 0 M.empty) "" input of
       Left err -> fail (show err)
       Right x -> return x

wrap :: Parse a -> Parse a
wrap p
  = do
      whiteSpace
      x <- p
      eof
      return x

literal :: Parse Literal
literal = (integer >>= return . LitI . fromInteger)
        <|>
          (do char '\''
              c <- lower <|> upper
              return $ LitC c)
        <|>
          (parseString >>= return . LitS)
        <|>
          (reserved "LOCALS" >> return LitLocalsAddress)
        <|>
          (reserved "ARGS" >> return LitArgsAddress)

compOp :: Parse CompOp
compOp = (reserved "EQ" >> return CEq)
       <|>
         (reserved "NEQ" >> return CNEq)
       <|>
         (reserved "LTE" >> return CLEq)
       <|>
         (reserved "GTE" >> return CGEq)
       <|>
         (reserved "LEQ" >> return CLEq)
       <|>
         (reserved "GEQ" >> return CGEq)
       <|>
         (reserved "LT" >> return CLT)
       <|>
         (reserved "GT" >> return CGT)

commands :: Parse Program
commands = do cs <- many command
              st <- getState
              return $ Program cs (lmap st)

command :: Parse Command
command = do whiteSpace
             c <- cmd <|> lab
             st0 <- getState
             updateState (\st -> st { cmdPosition = cmdPosition st0 + 1 } )
             return c
        where
          cmd = do c <- command_
                   whiteSpace
                   reservedOp ";"
                   return c

          lab = do id <- identifier --many (lower <|> upper)
                   whiteSpace
                   reservedOp ":"
                   st <- getState
                   updateState (\st' -> st' {
                                   lmap = M.insert id
                                                   (cmdPosition st)
                                                   (lmap st) } )
                   return $ Label id (cmdPosition st)

command_ :: Parse Command
command_ = (reserved "NOP" >> return Nop)
        <|>
          (reserved "HALT" >> return Halt)
        <|>
          (reserved "RETURN" >> return Return)
        <|>
          (reserved "PUSH" >> literal >>= return . Push)
        <|>
          (reserved "POP" >> return Pop)
        <|>
          (reserved "DUMPSTACK" >> return DumpStack)
        <|>
          (reserved "OUT" >> return Out)
        <|>
          (reserved "OUTLN" >> return OutLN)
        <|>
          (reserved "IN" >> return In)
        <|>
          (reserved "FLUSH" >> return Flush)
        <|>
          (reserved "STORE" >> return Store)
        <|>
          (reserved "STOREIDX" >> return StoreIdx)
        <|>
          (reserved "LOAD" >> return Load)
        <|>
          (reserved "LOADIDX" >> return LoadIdx)
        <|>
          (reserved "MOVRET" >> return MovRet)
        <|>
          (reserved "ALLOCA" >> return AllocA)
        <|>
          (reserved "PUSHARG" >> return PushArg)
        <|>
          (reserved "PUSHRET" >> return PushRet)
        <|>
          (reserved "COMPL" >> return Compl)
        <|>
          (reserved "DUP" >> return Dup)
        <|>
          (reserved "SWAP" >> return Swap)
        <|>
          (reserved "ADDSTACK" >> return Swap)
        <|>
          (compOp >>= return . Comp)
        <|>
          (reserved "MINUS" >> return Minus)
        <|>
          (reserved "MUL" >> return Mul)
        <|>
          (reserved "DIV" >> return Div)
        <|>
          (reserved "ADD" >> return Add)
        <|>
          (reserved "AND" >> return And)
        <|>
          (reserved "OR" >> return Or)
        <|>
          (reserved "NOT" >> return Not)
        <|>
          (reserved "JMPNZ" >> return JmpNZ)
        <|>
          (reserved "JMP" >> return Jmp)
        <|>
          (reserved "SUBCALL" >> return Subcall)
        <|>
          (do reservedOp "&"
              whiteSpace
              id <- identifier
              return $ AddrOf id)

langDef :: P.LanguageDef st
langDef = haskellDef {
                 P.reservedOpNames = [":", "&", ";"]
               , P.reservedNames  = ["PUSH", "MINUS", "ADD", "ADDSTACK", "NOP", "COMPL",
                                     "HALT", "RETURN", "OUT", "STORE", "STOREIDX",
                                     "DUP", "SWAP", "AND", "OR", "NOT", "SP", "FLUSH",
                                     "LOAD", "LOADIDX", "JMPNZ", "JMP", "EQ",
                                     "LEQ", "GEQ", "LT", "GT"]
               }

lexer       = P.makeTokenParser langDef

escape :: Parse String
escape = do
    d <- char '\\'
    c <- oneOf "\\\"0nrvtbf" -- all the characters which can be escaped
    return [d, c]

nonEscape :: Parse Char
nonEscape = noneOf "\\\"\0\n\r\v\t\b\f"

character :: Parse String
character = fmap return nonEscape <|> escape

parseString :: Parse String
parseString = do
    char '"'
    strings <- many character
    char '"'
    return $ concat strings

parens      = P.parens lexer
squares     = P.squares lexer
integer     = P.integer lexer
reservedOp  = P.reservedOp lexer
reserved    = P.reserved lexer
identifier  = P.identifier lexer
dot      = P.dot lexer
whiteSpace  = P.whiteSpace lexer
lexeme      = P.whiteSpace lexer
symbol      = P.symbol lexer
commaSep    = P.commaSep lexer
semiSep     = P.semiSep lexer
